# Contributing

You can contribute to STARRED in many ways and every submission will be greatly appreciated. 

## Reporting issues or problems

Please use [GitLab issues](https://gitlab.com/cosmograil/starred/-/issues) to report bugs. To do so, please include:

* The name and version of your operating system.
* Detailed steps to reproduce the bug.

## Implementing new features 

Please use [merge requests](https://gitlab.com/cosmograil/starred/-/merge_requests) if you want to modify the master branch. 

* Merge requests should include [automated tests](https://gitlab.com/cosmograil/starred/-/tree/main/tests).
* The added functions should include their corresponding docstrings.

## Reaching out to code developers

The list of authors and their email addresses are available [here](https://gitlab.com/cosmograil/starred/-/blob/main/AUTHORS.md).
