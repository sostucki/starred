Frequently Asked Questions
============

(Under construction)

Which units should I use for my data?
-------------------------------------
Estimating the noise properly is crucial for STARRED to work properly. You can use any units as long as the provided noise maps match the units of your data. However, we recommend to convert your data in electron before passing it to STARRED. In these units, you can easily estimate the Poisson noise by taking :math:`\sigma_p = \sqrt{Data}`.
The total noise variance is then:

.. math::
    \sigma^2 = \sigma_p^2 + \sigma_{background}^2

:math:`\sigma_{background}` can be easily estimated by measuring the standard deviation of an empty region of the image.
Note that you can refine the estimation of the noise level directly from the STARRED model (see `Notebook 1 <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/1_WFI%20narrow%20PSF%20generation.ipynb?ref_type=heads>`_ for an example)

**Warning** : this works only if all pixels have the same exposure time.
Most space-based images have different exposure time for each pixel.
You should then reconstruct the noise maps from the exposure maps to make sure that the Poisson noise is correctly estimated.

Should I tune parameters ``lambda_scales`` and ``lambda_hf``?
-------------------------------------------------------------
``lambda_scales`` and ``lambda_hf`` are the two most important parameters of STARRED as they control the regularization strength. They are expressed in the units of the combined effective
noise of all your images. You can change these two parameters to avoid under or over-fitting the data.

**PSF reconstruction:** If your noise is properly estimated, using ``lambda_hf`` equal to 3 means that you will use the
Starlet coefficients that are 3 :math:`\sigma` above the noise level in the smallest Starlet scale. Similarly, ``lambda_scales`` controls
the regularization in the remaining Starlet scales. Taking both ``lambda_hf`` and ``lambda_scales`` equal to 2 should give a :math:`\chi^2` of approximately 1,
indicating that you are neither under-fitting nor over-fitting the data.

**Deconvolution:**  Similarly to the PSF reconstruction case, setting both ``lambda_hf`` and ``lambda_scales`` to 2 should be a good guess.
If your data is not too noisy, you can completely cut the high-frequencies by setting ``lambda_hf`` to a very high number (1000 for example).
This will make data denoising more aggressive, but you will also suppress small structures in your reconstructed image.

Should I rescale my data?
-------------------------
Absolutely yes. All gradient-based optimization algorithms implemented in STARRED work much better if your data is between 0 and 1 (or something close to it).

..
Why using multi-step optimization?
----------------------------------

STARRED features some high-level functions that perform a sequence of fits, first fixing some of the parameters of the model
before running a full optimization with all degrees of freedom. These functions are in the ``prodedure`` subpackage.
For example, for the PSF reconstruction it is recommended to first fit a Moffat and then add a pixelated correction.
This will speed up the convergence and make the fitting procedure much more robust.

You can do that automatically with this piece of code:

..  code-block:: python

    from starred.procedures.psf_routines import run_multi_steps_PSF_reconstruction


    fitting_sequence = [['background'],['moffat']]
    optim_list = ['l-bfgs-b', 'adabelief']

    model, parameters, loss, kwargs_partial_list, LogL_list, loss_history_list = run_multi_steps_PSF_reconstruction(data, model,
                                                                                                parameters, sigma_2, masks,
                                                                                                lambda_scales=lambda_scales,
                                                                                                lambda_hf=lambda_hf, fitting_sequence = fitting_sequence,
                                                                                                optim_list = optim_list)

Here the first fit keeps the background fixed and uses the optimizer `l-bfgs-b`. The second fit will use `adabelief` with constant Moffat parameters, but the background will be adjusted.

A similar function is available for the deconvolution in the ``starred.procedures.deconvolution_routines`` module.
Deconvolution might converge faster if you first fix the astrometry of the point sources before running the full optimization.
You can do that by passing the following fitting sequence to the ``starred.procedures.deconvolution_routines.multi_steps_deconvolution()`` function:

.. code-block:: python

    fitting_sequence = [['pts-source-astrometry'],[]]

Other fixing options are `pts-source-astrometry`, `background`, and `sersic`.

How can I check that I am really using a GPU?
---------------------------------------------

You can add the following lines on top of your notebook:

.. code-block:: python

    from jax.lib import xla_bridge
    yourdevice = xla_bridge.get_backend().platform
    print(f"You are running on {yourdevice}.")

STARRED is about 20 times faster on GPU.
