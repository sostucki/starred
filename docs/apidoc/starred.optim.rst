starred\.optim package
======================

Submodules
----------

starred.optim.inference_base module
----------------------------------

.. automodule:: starred.optim.inference_base
    :members:
    :undoc-members:
    :show-inheritance:

starred.optim.optimization module
----------------------------------

.. automodule:: starred.optim.optimization
    :members:
    :undoc-members:
    :show-inheritance:

starred.optim.sampling module
----------------------------------

.. automodule:: starred.optim.sampling
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: starred.optim
    :members:
    :undoc-members:
    :show-inheritance: