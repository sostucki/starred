Tutorial
========
The basic functionality of STARRED are explained in this example notebook:

- `PSF reconstruction and deconvolution with STARRED <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/start_here.ipynb>`_

This is a good place to start but if you want to understand the more advanced capabilities of STARRED you can
go through the following examples:

- `Ground-based narrow PSF generation <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/1_WFI%20narrow%20PSF%20generation.ipynb>`_

- `Ground-based joint deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/2_DESJ0602-4335%20joint%20deconvolution.ipynb>`_

- `Another ground-based joint deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooksmore_examples//3_Another%20lensed%20quasar%20-%20joint%20deconvolution.ipynb>`_

- `James Webb Space Telescope (JWST) PSF generation and deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/4_JWST%20deconvolution.ipynb>`_

- `DES2038 joint deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/5_DES2038_from_WFI_joint_deconvolution.ipynb>`_

- `HST PSF reconstruction <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/6_HST-PSF%20reconstruction.ipynb>`_

- `JWST PSF reconstruction <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/more_examples/7_JWST-PSF_reconstruction.ipynb?ref_type=heads>`_

We will also keep posting new examples in this auxillary repository:

- `STARRED Examples <https://gitlab.com/cosmograil/starred-examples>`_

.. toctree::
	:maxdepth: 5
	:numbered:
	